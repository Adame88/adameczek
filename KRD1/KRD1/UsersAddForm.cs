﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace KRD1
{
    public partial class UsersAddForm : Form
    {
        private readonly UserClass _user;
  
        public UsersAddForm(UserClass user)
        {
            try
            {
                _user = user;
                InitializeComponent();

                textBoxName.Text = user.FirstName;
                textBoxSurname.Text = user.LastName;
                textBoxAdress.Text = user.Adress;
                comboBoxStatus.Text = user.Status;
                textBoxPassword.Text = user.Password;

                Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public UsersAddForm()
        {
            InitializeComponent();
           Show();
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        public void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        public void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        public void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            var users = XmlHelper.DeserializeFromXml();

            new UsersManagmentForm(users);
            Close();
        }

        private void UsersAddForm_Load(object sender, EventArgs e)
        {
           
        }

        private void buttonAddUser_Click(object sender, EventArgs e)
        {
            var _users = new List<UserClass>();

            var ID = _iDCreator(_users);

            if(_user != null)
            XmlHelper.RemoveUser(_users, _user);

            try
            {
                
                var _user = new UserClass
                {
                    ID = ID,
                    FirstName = textBoxName.Text,
                    LastName = textBoxSurname.Text,
                    Adress = textBoxAdress.Text,
                    Status = comboBoxStatus.Text,
                    Password = textBoxPassword.Text

                };
                XmlHelper.AddUsersToXml(_users, _user);

                var users = XmlHelper.DeserializeFromXml();

                new UsersManagmentForm(users);
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void ButtonRemove_Click(object sender, EventArgs e)
        {
            try
            {
                var _users = new List<UserClass>();

                XmlHelper.RemoveUser(_users, _user);

                var users = XmlHelper.DeserializeFromXml();

                new UsersManagmentForm(users);
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static int _iDCreator(List<UserClass> _list)
        {
            var ID = 0;
            _list = XmlHelper.DeserializeFromXml();
            ID = _list.Max(t => t.ID);
            ID++;
            return ID;
        }

        private void textBoxPassword_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
