﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KRD1
{
    public partial class MenuForm : Form
    {
        private readonly UserClass _resultUser;

        public MenuForm(UserClass resultUser)
        {
            _resultUser = resultUser;

            InitializeComponent();
            Show();

            var postList = XmlHelper.DeserializePostFromXml();

            if (resultUser.Status == "Administrator")
            {
                UsersManagmentButton.Show();
            }
            if (resultUser.Status == "Kurier")
            {
             
                new CourierPanel(resultUser, postList);
                Close();
            }

            if (resultUser.Status == "Urzytkownik")
            {
                UsersManagmentButton.Hide();
                new UserPackView(postList);
                Close();
            }
        }
        
        private void UsersManagmentButton_Click(object sender, EventArgs e)
        {
            var users = XmlHelper.DeserializeFromXml();

            new UsersManagmentForm(users);

            Close();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            new Form1();
            Close();
        }

        private void packManagmantButton_Click(object sender, EventArgs e)
        {
            var postsList = XmlHelper.DeserializePostFromXml();

            new CourierPanel(_resultUser, postsList);

            Close();
        }
    }
}
