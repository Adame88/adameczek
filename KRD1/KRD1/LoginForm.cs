﻿using System;
using System.Windows.Forms;

namespace KRD1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Show();
        }

       

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        
        private void loginButton_Click_1(object sender, EventArgs e)
        {
            var _userList = XmlHelper.DeserializeFromXml();

            var ResultUser =
                _userList.Find(x => x.FirstName.Equals(SurnameBox.Text) && x.Password.Equals(PasswordBox.Text));

            
                if(ResultUser != null )
                {
                    new MenuForm(ResultUser);
                    Hide();
                }
                else
                {
                    MessageBox.Show(" Niepoprawny login lub hasło !");
                }
         
               
            
        }
    }
}
