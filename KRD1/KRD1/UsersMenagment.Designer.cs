﻿namespace KRD1
{
    partial class UsersMenagment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Dodaj = new System.Windows.Forms.Button();
            this.Usuń = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.szukaj = new System.Windows.Forms.Button();
            this.Edit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Dodaj
            // 
            this.Dodaj.Location = new System.Drawing.Point(12, 12);
            this.Dodaj.Name = "Dodaj";
            this.Dodaj.Size = new System.Drawing.Size(99, 55);
            this.Dodaj.TabIndex = 0;
            this.Dodaj.Text = "Dodaj";
            this.Dodaj.UseVisualStyleBackColor = true;
            this.Dodaj.Click += new System.EventHandler(this.Dodaj_Click);
            // 
            // Usuń
            // 
            this.Usuń.Location = new System.Drawing.Point(430, 12);
            this.Usuń.Name = "Usuń";
            this.Usuń.Size = new System.Drawing.Size(99, 55);
            this.Usuń.TabIndex = 2;
            this.Usuń.Text = "Usuń";
            this.Usuń.UseVisualStyleBackColor = true;
            this.Usuń.Click += new System.EventHandler(this.Usuń_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 123);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(517, 150);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 97);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(448, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // szukaj
            // 
            this.szukaj.Location = new System.Drawing.Point(466, 94);
            this.szukaj.Name = "szukaj";
            this.szukaj.Size = new System.Drawing.Size(75, 23);
            this.szukaj.TabIndex = 5;
            this.szukaj.Text = "Szukaj";
            this.szukaj.UseVisualStyleBackColor = true;
            this.szukaj.Click += new System.EventHandler(this.szukaj_Click);
            // 
            // Edit
            // 
            this.Edit.Location = new System.Drawing.Point(200, 12);
            this.Edit.Name = "Edit";
            this.Edit.Size = new System.Drawing.Size(99, 55);
            this.Edit.TabIndex = 6;
            this.Edit.Text = "Edytuj";
            this.Edit.UseVisualStyleBackColor = true;
            this.Edit.Click += new System.EventHandler(this.Edit_Click);
            // 
            // UsersMenagment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 376);
            this.Controls.Add(this.Edit);
            this.Controls.Add(this.szukaj);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.Usuń);
            this.Controls.Add(this.Dodaj);
            this.Name = "UsersMenagment";
            this.Text = "UsersMenagment";
            this.Load += new System.EventHandler(this.UsersMenagment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Dodaj;
        private System.Windows.Forms.Button Usuń;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button szukaj;
        private System.Windows.Forms.Button Edit;
    }
}