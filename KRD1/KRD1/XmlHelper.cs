﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;


namespace KRD1
{
    public static class XmlHelper
    {
        public static void AddUsersToXml(this List<UserClass> _users, UserClass user)
        {
            var _xmlSerializer = new XmlSerializer(typeof(List<UserClass>));
            using (var sr = new StreamReader("test.xml"))
            {
                _users = (List<UserClass>)_xmlSerializer.Deserialize(sr);
            }
            using (var streamWriter = new StreamWriter("test.xml"))
            {
                _users.Add(user);

                _xmlSerializer.Serialize(streamWriter, _users);
            }
        }

        public static List<UserClass> DeserializeFromXml()
        {
            List<UserClass> userList = new List<UserClass>();
            var xmlSerializer = new XmlSerializer(typeof(List<UserClass>));
            using (var sr = new StreamReader("test.xml"))
            {
                userList = (List<UserClass>)xmlSerializer.Deserialize(sr);
            }
            return userList;
        }

        public static void RemoveUser(List<UserClass> _users, UserClass user)
        {
            var _xmlSerializer = new XmlSerializer(typeof(List<UserClass>));
            using (var sr = new StreamReader("test.xml"))
            {
                _users = (List<UserClass>)_xmlSerializer.Deserialize(sr);
            }
        
            var userToRemove = _users.Find(x => x.ID.Equals(user.ID));

            if (userToRemove != null)
            {
                _users.Remove(userToRemove);

                using (var streamWriter = new StreamWriter("test.xml"))
                {
                    _xmlSerializer.Serialize(streamWriter, _users);
                }
            }
        }

        public static void AddPackToXml(this List<PackClass> _packs, PackClass pack)
        {
            var _xmlSerializer = new XmlSerializer(typeof(List<PackClass>));
            using (var sr = new StreamReader("Postal.xml"))
            {
                _packs = (List<PackClass>)_xmlSerializer.Deserialize(sr);
            }
            using (var streamWriter = new StreamWriter("Postal.xml"))
            {
                _packs.Add(pack);

                _xmlSerializer.Serialize(streamWriter, _packs);
            }
        }

        public static List<PackClass> DeserializePostFromXml()
        {
            List<PackClass> packList = new List<PackClass>();
            var xmlSerializer = new XmlSerializer(typeof(List<PackClass>));
            using (var sr = new StreamReader("Postal.xml"))
            {
                packList = (List<PackClass>)xmlSerializer.Deserialize(sr);
            }
            return packList;
        }

        public static void RemovePost(List<PackClass> _packs, PackClass pack)
        {
            var _xmlSerializer = new XmlSerializer(typeof(List<PackClass>));
            using (var sr = new StreamReader("Postal.xml"))
            {
                _packs = (List<PackClass>)_xmlSerializer.Deserialize(sr);
            }

            var userToRemove = _packs.Find(x => x.ID.Equals(pack.ID));

            if (userToRemove != null)
            {
                _packs.Remove(userToRemove);

                using (var streamWriter = new StreamWriter("Postal.xml"))
                {
                    _xmlSerializer.Serialize(streamWriter, _packs);
                }
            }
        }
    }
}
