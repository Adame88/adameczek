﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KRD1
{
    public partial class UserPackView : Form
    {
        private readonly PackClass _result;

        private readonly List<PackClass> _packsList;

        public UserPackView(List<PackClass> packList)
        {
            _packsList = packList;

            InitializeComponent();
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;

            var dataSet = new DataSet();
            dataSet.ReadXml("Postal.xml");
            dataGridView1.DataSource = dataSet.Tables[0];

            Show();
        }

        public UserPackView(PackClass result)
        {
    
            _result = result;
            InitializeComponent();

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;

            var _userTable = new DataTable();

            _userTable.Columns.Add("ID", typeof(int));
            _userTable.Columns.Add("Status", typeof(string));
            _userTable.Columns.Add("ResponsibleCourier", typeof(string));

            _userTable.Rows.Add(result.ID, result.Status, result.ResponsibleCourier);

            var dataSet = new DataSet();

            dataSet.Tables.Add(_userTable);

            dataGridView1.DataSource = dataSet.Tables[0];

            Show();
        }

        private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void b(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var _wantedPost = int.Parse(textBox1.Text);

            var result = _packsList.Find(x =>
                x.ID.Equals(_wantedPost));

            if (result == null)
            {
                Exception exception = new SystemException();
                MessageBox.Show($"Nie ma takiej paczki " + exception);
            }
            if (result != null)
            {

                new UserPackView(result);
                Close();
            }
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            new Form1();
            Close();
        }
    }
}
