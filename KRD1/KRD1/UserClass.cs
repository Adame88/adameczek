﻿namespace KRD1
{
    public class UserClass
    {
        public int ID { get;  set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Adress { get; set; }
        public string Status { get; set; }
        public string Password { get; set; }
    }
}
