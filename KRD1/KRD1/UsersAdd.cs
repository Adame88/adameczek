﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace KRD1
{
    public partial class UsersAdd : Form
    {
        private readonly User _user;

        public UsersAdd(User user)
        {
            _user = user;
            InitializeComponent();
        }

        public UsersAdd()
        {
            InitializeComponent();
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)//Add new Users
        {
            var _usersList = new List<User>();
            try
            {
                var user = new User
                {
                    FirstName = textBox1.Text,
                    LastName = textBox2.Text,
                    Adress = textBox3.Text,
                    Status = comboBox1.Text
                };

                XmlHelper.AddUsersToXml(_usersList, user);


                var users = XmlHelper.DeserializeFromXml();

                new UsersMenagment(users);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        public void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        public void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        public void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var _usersList = new List<User>();

            try
            {
                var user = new User
                {
                    //FirstName = textBox1.Text,
                    //LastName = textBox2.Text,
                    //Adress = textBox3.Text,
                    //Status = comboBox1.Text
                };

                //XmlHelper.EditUsersToXml();




                this.Hide();
                //todo: odkomentować
                //var UM = new UsersMenagment();
                //UM.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

       
    }
}
