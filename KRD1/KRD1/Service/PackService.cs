﻿using KRD1.Service.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace KRD1.Service
{
    public class PackService : IPackService
    {
        private readonly Uri url;
        private readonly object loginReguest;

        //public void CreateAsync(PackClass newPack)
        //{
        //    throw new System.NotImplementedException();
        //}

        public async void CreateAsync(PackClass newPack)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(loginReguest), Encoding.UTF8, "application/json");
                var reqest = new HttpRequestMessage(HttpMethod.Post, url);
                reqest.Content = content;
                var result = await httpClient.SendAsync(reqest);
                if (result.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    return null;
                }
                var json = await result.Content.ReadAsByteArrayAsync();
                return JsonConvert.DeserializeObject<LoginRespons>(json);
            }
        }

        public async void DelateAsync(int id)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(loginReguest), Encoding.UTF8, "application/json");
                var reqest = new HttpRequestMessage(HttpMethod.Delete, url);
                reqest.Content = content;
                var result = await httpClient.SendAsync(reqest);
                if (result.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    return null;
                }
                var json = await result.Content.ReadAsByteArrayAsync();
                return JsonConvert.DeserializeObject<LoginRespons>(json);
            }
        }

        public async IList<PackClass> GetAll()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(), Encoding.UTF8, "p")
            }
        }

        public async PackClass GetById(int id)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(loginReguest), Encoding.UTF8, "application/json");
                var reqest = new HttpRequestMessage(HttpMethod.Get, url)
                {
                    Content = content
                };
                var result = await httpClient.SendAsync(reqest);
                if (result.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    return null;
                }
            }
        }

        public async void Update(PackClass updatePack)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(loginReguest), Encoding.UTF8, "application/json");
                var reqest = new HttpRequestMessage(HttpMethod.Post, url);
                reqest.Content = content;
                var result = await httpClient.SendAsync(reqest);
                if (result.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    return null;
                }
            }
        }
    }
}
