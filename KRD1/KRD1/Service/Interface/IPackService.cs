﻿using System.Collections.Generic;

namespace KRD1.Service.Interface
{
    interface IPackService
    {
        IList<PackClass> GetAll();

        PackClass GetById(int id);

        void DelateAsync(int id);

        void CreateAsync(PackClass newPack);

        void Update(PackClass updatePack);
    }
}
