﻿namespace KRD1
{
    partial class MenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UsersManagmentButton = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // UsersManagmentButton
            // 
            this.UsersManagmentButton.Location = new System.Drawing.Point(12, 12);
            this.UsersManagmentButton.Name = "UsersManagmentButton";
            this.UsersManagmentButton.Size = new System.Drawing.Size(113, 45);
            this.UsersManagmentButton.TabIndex = 0;
            this.UsersManagmentButton.Text = "Zarządzaj Urzytkoniwkami";
            this.UsersManagmentButton.UseVisualStyleBackColor = true;
            this.UsersManagmentButton.Click += new System.EventHandler(this.UsersManagmentButton_Click);
            // 
            // Exit
            // 
            this.Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Exit.Location = new System.Drawing.Point(168, 12);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(113, 45);
            this.Exit.TabIndex = 1;
            this.Exit.Text = "Exit";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // MenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 131);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.UsersManagmentButton);
            this.Name = "MenuForm";
            this.Text = "Menu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button UsersManagmentButton;
        private System.Windows.Forms.Button Exit;
    }
}