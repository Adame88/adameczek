﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KRD1
{
    public partial class PackEdit : Form
    {
        private readonly PackClass _pack;

        private UserClass _resultUser;

        public PackEdit(UserClass resultUser)
        {
            _resultUser = resultUser;
          
            InitializeComponent();

            Show();
        }

        public PackEdit(UserClass resultUser, PackClass pack)
        {
            _resultUser = resultUser;
            _pack = pack;

            InitializeComponent();

            statusComboBox.Text = pack.Status;

            Show();
        }

        private void AddEditButton_Click(object sender, EventArgs e)
        {
            var _postsList = XmlHelper.DeserializePostFromXml();

            if (_pack != null)
            XmlHelper.RemovePost(_postsList, _pack);

            var ID = _iDCreator(_postsList);

            try
            {
                var _post = new PackClass()
                {
                    ID = ID,
                    Status = statusComboBox.Text,
                    ResponsibleCourier = AddCourierToPost(_resultUser)

                };
                XmlHelper.AddPostToXml(_postsList, _post);

                //var posts = XmlHelper.DeserializePostFromXml();

                new CourierPanel(_resultUser, _postsList);
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            try
            {
                var _postsList = XmlHelper.DeserializePostFromXml();
                XmlHelper.RemovePost(_postsList, _pack);


                new CourierPanel(_resultUser, _postsList);
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            var postsList = XmlHelper.DeserializePostFromXml();

            new CourierPanel(_resultUser, postsList);

            Close();
        }

        private static int _iDCreator(List<PackClass> _list)
        {
            var ID = 0;
            _list = XmlHelper.DeserializePostFromXml();
            ID = _list.Max(t => t.ID);
            ID++;
            return ID;
        }

        private static string AddCourierToPost(UserClass _resultUser)
        {
            var responsible = _resultUser.LastName;
            return responsible;
        }
    }
}


