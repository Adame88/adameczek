﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KRD1
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
            this.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var users = XmlHelper.DeserializeFromXml();

            new UsersMenagment(users);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
