﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;


namespace KRD1
{
    public partial class UsersManagmentForm : Form
    {
        private readonly List<UserClass> _users;

        private readonly UserClass result;
       
        public UsersManagmentForm(List<UserClass> users)
        {
            _users = users;
            
            InitializeComponent();
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;

            var dataSet = new DataSet();
            dataSet.ReadXml("test.xml");
            dataGridView1.DataSource = dataSet.Tables[0];

            Show();
        }

        public UsersManagmentForm(UserClass _result)
        {
            result = _result;

            InitializeComponent();
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;

            var _userTable = new DataTable();

            _userTable.Columns.Add("ID", typeof(int));
            _userTable.Columns.Add("FirstName", typeof(string));
            _userTable.Columns.Add("LastName", typeof(string));
            _userTable.Columns.Add("Adress", typeof(string));
            _userTable.Columns.Add("Status", typeof(string));
            _userTable.Columns.Add("hasło", typeof(string));

            _userTable.Rows.Add(result.ID, result.FirstName, result.LastName, result.Adress, result.Status);
           


            var dataSet = new DataSet();

            dataSet.Tables.Add(_userTable);
 
            dataGridView1.DataSource = dataSet.Tables[0];

            Show();
        }

        private void szukaj_Click(object sender, EventArgs e)
        {
            var _list = XmlHelper.DeserializeFromXml();

            var _wantedUser = searchBox.Text;

            string[] dates = _wantedUser.Split(' ');
            string _firstName = dates[0];
            string _lastName = dates[1];

            var result = _list.Find(x =>
                x.FirstName.Equals(_firstName) || x.LastName.Equals(_lastName));

            if (result == null)
            {
                Exception exception = new SystemException();
                MessageBox.Show($"Nie ma takiego urzytkownika " + exception);
            }
            if (result != null)
            {
              
                new UsersManagmentForm(result);
                Close();
            }
        }

        private void searchBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void ButtonAddUser_Click(object sender, EventArgs e)
        {
            new UsersAddForm();
            Close();
        }

        private void UsersManagmentForm_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];

                var user = new UserClass
                {
                    ID = int.Parse(row.Cells[0].Value.ToString()),
                    FirstName = row.Cells[1].Value.ToString(),
                    LastName = row.Cells[2].Value.ToString(),
                    Adress = row.Cells[3].Value.ToString(),
                    Status = row.Cells[4].Value.ToString(),
                    Password = row.Cells[5].Value.ToString()
                };

                new UsersAddForm(user);
                Hide();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
             
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void infoLabel_Click(object sender, EventArgs e)
        {

        }

        private void Exit_Click(object sender, EventArgs e)
        {
            new Form1();
            Close();
        }
    }
}
