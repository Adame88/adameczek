﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace KRD1
{
    public partial class CourierPanel : Form
    {
        private readonly List<PackClass> _packsList;

        private readonly PackClass _pack;

        private UserClass _resultUser;

        private readonly PackClass _result;

        public CourierPanel(UserClass resultUser, List<PackClass> postsList)
        {
            _resultUser = resultUser;
            _packsList = postsList;
            InitializeComponent();

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;

            var dataSet = new DataSet();
            dataSet.ReadXml("Postal.xml");
            dataGridView1.DataSource = dataSet.Tables[0];

            Show();
        }

        public CourierPanel(PackClass result, UserClass resultUser)
        {
            _resultUser = resultUser;
            _result = result;
            InitializeComponent();

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;

            var _userTable = new DataTable();

            _userTable.Columns.Add("ID", typeof(int));
            _userTable.Columns.Add("Status", typeof(string));
            _userTable.Columns.Add("ResponsibleCourier", typeof(string));

            _userTable.Rows.Add(result.ID, result.Status, result.ResponsibleCourier);

            var dataSet = new DataSet();

            dataSet.Tables.Add(_userTable);

            dataGridView1.DataSource = dataSet.Tables[0];

            Show();
        }

        private void DataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];

                var post = new PackClass()
                {
                    ID = int.Parse(row.Cells[0].Value.ToString()),
                    Status = row.Cells[1].Value.ToString(),
                    ResponsibleCourier = row.Cells[2].Value.ToString()

                };

                new PackEdit(_resultUser, post);
                Hide();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            new Form1();
            Close();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            new PackEdit(_resultUser);
            Hide();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            var _wantedPost = int.Parse(textBox1.Text);

            var result = _packsList.Find(x =>
                x.ID.Equals(_wantedPost));

            if (result == null)
            {
                Exception exception = new SystemException();
                MessageBox.Show($"Nie ma takiej paczki " + exception);
            }
            if (result != null)
            {

                new CourierPanel(result, _resultUser);
                Close();
            }
        }
    }
}
