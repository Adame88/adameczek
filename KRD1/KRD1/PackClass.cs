﻿namespace KRD1
{
    public class PackClass
    {
        public int ID { get; set; }
        public string Status { get; set; }
        public string ResponsibleCourier { get; set; }
    }
}
