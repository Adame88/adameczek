﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebKRD2.Infrastructure.Repository.Interfaces;
using WebKRD2.Infrastructure.Service.Interfaces;
using WebKRD2.Models;

namespace WebKRD2.Infrastructure.Service
{
    public class PackService : IPackService
    {
        private readonly IPackRepository _packRepository;

        public PackService(IPackRepository packRepository)
        {
            if (packRepository == null)
            {
                throw new ArgumentNullException("PostRepository nie moze byc nullem!");
            }

            _packRepository = packRepository;
        }

        public IList<PackModel> GetAll()
        {
            return _packRepository.GetAll();
        }

        public PackModel GetById(int id)
        {
            return _packRepository.GetById(id);
        }

        public void Create(PackModel post)
        {
            if (post == null)
            {
                throw new ArgumentNullException("Paczka nie moze byc nullem!");
            }

            _packRepository.Create(post);
        }

        public void Delete(int id)
        {
            var postToDelete = _packRepository.GetById(id);
            _packRepository.Remove(postToDelete);
        }

        public void Update(PackModel oldPack)
        {
            _packRepository.Update(oldPack);
        }
    }
}
