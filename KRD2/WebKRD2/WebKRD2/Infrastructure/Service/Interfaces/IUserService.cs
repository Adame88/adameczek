﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebKRD2.Models;

namespace WebKRD2.Infrastructure.Service.Interfaces
{
    public interface IUserService
    {
        IList<UserModel> GetAll();

        UserModel GetById(int id);

        void Delete(int id);

        void Create(UserModel user);

        void Update(UserModel userToUpdate);
    }
}
