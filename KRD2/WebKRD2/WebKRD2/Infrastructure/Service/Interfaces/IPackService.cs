﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebKRD2.Models;

namespace WebKRD2.Infrastructure.Service.Interfaces
{
    public interface IPackService
    {
        IList<PackModel> GetAll();

        PackModel GetById(int id);

        void Delete(int id);

        void Create(PackModel pack);

        void Update(PackModel updatePacl);
    }
}
