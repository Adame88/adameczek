﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using WebKRD2.Infrastructure.Repository.Interfaces;
using WebKRD2.Infrastructure.Service.Interfaces;
using WebKRD2.Models;

namespace WebKRD2.Infrastructure.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            if (userRepository == null)
            {
                throw new ArgumentNullException("PostRepository nie moze byc nullem!");
            }

            _userRepository = userRepository;
        }

        public IList<UserModel> GetAll()
        {
            return _userRepository.GetAll();
        }

        public UserModel GetById(int id)
        {
            return _userRepository.GetById(id);
        }

        public void Create(UserModel user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("Post nie moze byc nullem!");
            }

            _userRepository.Create(user);
        }

        public void Delete(int id)
        {
            var userToDelete = _userRepository.GetById(id);
            _userRepository.Remove(userToDelete);
        }

        public void Update(UserModel userToUpdate)
        {
            _userRepository.Update(userToUpdate);
        }
    }
}
