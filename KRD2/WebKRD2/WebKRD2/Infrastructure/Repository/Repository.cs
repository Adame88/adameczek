﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebKRD2.Infrastructure.DataAccess;
using WebKRD2.Infrastructure.Repository.Interfaces;
using WebKRD2.Models;

namespace WebKRD2.Infrastructure.Repository
{
    public class Repository<T> : IRepository<T> where T : BaseEntitiy
    {
        protected readonly DbContextModel Context;

        public Repository(DbContextModel context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("BlogContext nie moze byc nullem");
            }

            Context = context;
            context.Database.EnsureCreated();
        }

        public void Create(T entity)
        {
            Context.Set<T>().Add(entity);
            Context.SaveChanges();
        }

        public void Remove(T entity)
        {
            Context.Set<T>().Remove(entity);
            Context.SaveChanges();
        }

        public T GetById(int id)
        {
            return
                Context.Set<T>().FirstOrDefault(x => x.ID == id);
        }

        public IList<T> GetAll()
        {
            return
                Context.Set<T>().ToList();
        }

        public void Update(T entity)
        {
            Context.Set<T>().Update(entity);
            Context.SaveChanges();
        }


    }
}
