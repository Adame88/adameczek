﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebKRD2.Models;

namespace WebKRD2.Infrastructure.Repository.Interfaces
{
    public interface IUserRepository
    {
        void Create(UserModel user);

        void Remove(UserModel userToRemove);

        UserModel GetById(int id);

        IList<UserModel> GetAll();

        void Update(UserModel userToUpdate);
    }
}
