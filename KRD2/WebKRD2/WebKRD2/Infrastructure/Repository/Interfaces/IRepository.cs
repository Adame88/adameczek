﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebKRD2.Models;

namespace WebKRD2.Infrastructure.Repository.Interfaces
{
    public interface IRepository<T> where T : BaseEntitiy
    {
        void Create(T entity);

        void Remove(T entity);

        T GetById(int id);

        IList<T> GetAll();

        void Update(T entity);
    }
}
