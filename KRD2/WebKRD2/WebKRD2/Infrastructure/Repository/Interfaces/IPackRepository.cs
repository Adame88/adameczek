﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebKRD2.Models;

namespace WebKRD2.Infrastructure.Repository.Interfaces
{
    public interface IPackRepository
    {
        void Create(PackModel pack);

        void Remove(PackModel packToRemove);

        PackModel GetById(int id);

        IList<PackModel> GetAll();

        void Update(PackModel updatePack);
    }
}
