﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebKRD2.Infrastructure.DataAccess;
using WebKRD2.Infrastructure.Repository.Interfaces;
using WebKRD2.Models;

namespace WebKRD2.Infrastructure.Repository
{
    public class PackRepository : Repository<PackModel>, IPackRepository
    {
        public PackRepository(DbContextModel context) : base(context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("BlogContext nie moze byc nullem");
            }
        }


        public void Create(PackModel pack)
        {
            Context.Set<PackModel>().Add(pack);
            Context.SaveChanges();
        }

        public void Remove(PackModel packToRemove)
        {
            Context.Set<PackModel>().Remove(packToRemove);
            Context.SaveChanges();
        }

        public PackModel GetById(int id)
        {
            return
                Context.Set<PackModel>().FirstOrDefault(x => x.ID == id);
        }

        public IList<PackModel> GetAll()
        {
            return
                Context.Set<PackModel>().ToList();
        }

        public void Update(PackModel updatePack)
        {
            Context.Set<PackModel>().Update(updatePack);
            Context.SaveChanges();
        }
    }
}
