﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebKRD2.Infrastructure.DataAccess;
using WebKRD2.Infrastructure.Repository.Interfaces;
using WebKRD2.Models;

namespace WebKRD2.Infrastructure.Repository
{
    public class UserRepository : Repository<UserModel>, IUserRepository
    {
        public UserRepository(DbContextModel context) : base(context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("DbContextModel nie moze byc nullem");
            }
        }

        public void Create(UserModel user)
        {
            Context.Set<UserModel>().Add(user);
            Context.SaveChanges();
        }

        public void Remove(UserModel userToRemove)
        {
            Context.Set<UserModel>().Remove(userToRemove);
            Context.SaveChanges();
        }

        public UserModel GetById(int id)
        {
            return
                Context.Set<UserModel>().FirstOrDefault(x => x.ID == id);
        }

        public IList<UserModel> GetAll()
        {
            return
                Context.Set<UserModel>().ToList();
        }

        public void Update(UserModel userToUpdate)
        {
            Context.Set<UserModel>().Update(userToUpdate);
            Context.SaveChanges();
        }
    }
}
