﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebKRD2.Models;

namespace WebKRD2.Infrastructure.Configurations
{
    public class PackConfiguration : IEntityTypeConfiguration<PackModel>
    {
        public void Configure(EntityTypeBuilder<PackModel> builder)
        {
            builder.ToTable(("Packs"));
        }
    }
}
