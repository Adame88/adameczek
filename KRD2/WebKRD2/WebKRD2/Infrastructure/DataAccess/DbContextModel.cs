﻿using Microsoft.EntityFrameworkCore;
using WebKRD2.Models;

namespace WebKRD2.Infrastructure.DataAccess
{
    public class DbContextModel :DbContext
    {
        public DbContextModel(DbContextOptions<DbContextModel> options) : base(options)
        {
        }

        public DbSet<PackModel>PostModels { get; set; }

        public DbSet<UserModel>UserModels { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PackModel>().ToTable("PostModel");
            modelBuilder.Entity<UserModel>().ToTable("UserModel");
        }
    }
}
