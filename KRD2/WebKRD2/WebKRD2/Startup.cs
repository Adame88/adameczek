﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebKRD2.Infrastructure.DataAccess;
using WebKRD2.Infrastructure.Repository;
using WebKRD2.Infrastructure.Repository.Interfaces;
using WebKRD2.Infrastructure.Service;
using WebKRD2.Infrastructure.Service.Interfaces;

namespace WebKRD2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IPackRepository, PackRepository>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPackService, PackService>();

            var connection = Configuration.GetConnectionString("WebKRD2ConnectString");
            services.AddDbContext<DbContextModel>(options => options.UseSqlServer(connection));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
