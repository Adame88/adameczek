﻿using System.Linq;
using WebKRD2.Infrastructure.DataAccess;
using WebKRD2.Models;

namespace WebKRD2.Data
{
    public class DbInitializer
    {
        public static void Initialize(DbContextModel context)
        {
            context.Database.EnsureCreated();

            if (context.PostModels.Any())
            {
                return;
            }

            var postModel = new PackModel[]
            {
                new PackModel {Status = "W drodze", ResponsibleCourier = "Kurier"},
                new PackModel {Status = "Na magazynie", ResponsibleCourier = "Kurier"}
            };
            foreach (PackModel p in postModel)
            {
                context.PostModels.Add(p);
            }
            context.SaveChanges();

            var userModel = new UserModel[]
            {
                new UserModel
                {
                    FirstName = "Kurier",
                    LastName = "Kurier",
                    Adress = "Pietruszkowa 8",
                    Status = "Kurier",
                    Password = "123456"
                },
                new UserModel
                {
                    FirstName = "Adam",
                    LastName = "Dyksik",
                    Adress = "Grunwaldzka 6",
                    Status = "Administrator",
                    Password = "123456"
                },
                new UserModel
                {
                    FirstName = "Użytkownik",
                    LastName = "Użytkownik",
                    Adress = "Pierogowa 6/12",
                    Status = "Użytkownik"
                }
            };
            foreach (UserModel u in userModel)
            {
                context.UserModels.Add(u);
            }

            context.SaveChanges();
        }
    }
}
