namespace WebKRD2.Models
{
    public class UserModel : BaseEntitiy
    {
        public int ID { get; set; }
        public string FirstName {get; set;}
        public string LastName  {get; set;}
        public string Status {get ; set;}
        public string Adress {get; set;} 
        public string Password {get; set;}
    }
}