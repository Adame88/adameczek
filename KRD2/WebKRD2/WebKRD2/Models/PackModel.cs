using System;

namespace WebKRD2.Models
{
    public class PackModel : BaseEntitiy
    {
        public int ID { get; set; }
        public string Status { get; set; }
        public string ResponsibleCourier { get; set; }
    }
}