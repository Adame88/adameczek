﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebKRD2.Infrastructure.DataAccess;
using WebKRD2.Infrastructure.Service.Interfaces;
using WebKRD2.Models;

namespace WebKRD2.Controllers
{
    [Produces("application/json")]
    [Route("api/PostModel")]
    public class PostModelController : Controller
    {
        private readonly IPackService _packService;

        public PostModelController(IPackService packService)
        {
            _packService = packService;
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult GetAll()
        {
            var resultList = _packService.GetAll();
            return Json(resultList);
        }

        [HttpGet]
        [Route("[action]/{id}")]
        public IActionResult GetById(int id)
        {
            var resultPack = _packService.GetById(id);
            return Json(resultPack);
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult AddPack(string responsibleCourier, string status)
        {
            var newPost = new PackModel
            {
                Status = status,
                ResponsibleCourier = responsibleCourier
            };
            _packService.Create(newPost);

            return Ok();
        }

        [HttpPost]
        [Route("[action]/{id}")]
        public IActionResult UpdatePostById(PackModel updatePack)
        {
           _packService.Update(updatePack);
            return Ok();
        }

        [HttpDelete]
        [Route("[action]/{id}")]
        public IActionResult DeletePost(int id)
        {
            _packService.Delete(id);

            return Ok();
        }
    }
}