﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebKRD2.Infrastructure.DataAccess;
using WebKRD2.Infrastructure.Service.Interfaces;
using WebKRD2.Models;

namespace WebKRD2.Controllers
{
    [Produces("application/json")]
    [Route("api/UserModel")]
    public class UserModelController : Controller
    {
        private readonly IUserService _userService;

        public UserModelController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult GetAll()
        {
            var resultList = _userService.GetAll();
            return Json(resultList) ;
        }

        [HttpGet]
        [Route("[action]/{id}")]
        public IActionResult GetById(int id)
        {
            var resultUser = _userService.GetById(id);
            return Json(resultUser);
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult AddUser(string firstName, string lastName, string status, string adress, string password)
        {
            var newUser = new UserModel
            {
                FirstName = firstName,
                LastName = lastName,
                Status = status,
                Adress = adress,
                Password = password
            };
            
           _userService.Create(newUser);

            return Ok();
        }

        [HttpPost]
        [Route("[action]/{id}")]
        public IActionResult UpdateUser(UserModel updateUser)
        {
            _userService.Update(updateUser);
            return Ok();
        }

        [HttpDelete]
        [Route("[action]/{id}")]
        public IActionResult DeleteUser(int id)
        {
           _userService.Delete(id);
            return Ok();
        }
    }
}