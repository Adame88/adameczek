﻿namespace Steve.Common.Entities
{
    public class Pack
    {
        public int ID { get; set; }
        public string Status { get; set; }
        public string ResponsibleCourier { get; set; }

    }
}
